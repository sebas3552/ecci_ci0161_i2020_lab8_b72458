package cr.ac.ucr.ecci.cql.mycomputervision;

import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceView;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
public class MainActivity extends AppCompatActivity implements
        // Implementacion para recibir los frames de la camara
        CameraBridgeViewBase.CvCameraViewListener2 {
    private static final String TAG = "OpenCVSample::Activity";
    // Vistas
    private static final int VIEW_MODE_RGBA = 0;
    private static final int VIEW_MODE_GRAY = 1;
    private static final int VIEW_MODE_CANNY = 2;
    private static final int VIEW_MODE_EDGES = 3;
    // modo seleccionado
    private int mViewMode;
    private Mat mRgba;
    private Mat mIntermediateMat;
    private Mat mGray;
    // vista de camara de OpenCV para implementar la interaccion entre OpenCV y la camara
    private CameraBridgeViewBase mOpenCvCameraView;
    // opciones del menu
    private MenuItem mItemPreviewRGBA;
    private MenuItem mItemPreviewGray;
    private MenuItem mItemPreviewCanny;
    private MenuItem mItemPreviewEdges;

    // Este el objeto callback usado para OpenCV
    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        // Este metodo es llamado cuando OpenCV manager se conecta
        public void onManagerConnected(int status) {
            // Successfully connected
            // Se habilita la inereaccion con la camara
            if (status == LoaderCallbackInterface.SUCCESS) {
                Log.i(TAG, "OpenCV loaded successfully.");
                mOpenCvCameraView.enableView();
            } else {
                super.onManagerConnected(status);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // mantener la pantalla activa
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        //
        setContentView(R.layout.activity_main);
        // instanciamos
        mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.VisionView);
        // view visible
        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
        // Registramos la actividad como el objeto callback de los frames de la camara de OpenCV
        mOpenCvCameraView.setCvCameraViewListener(this);
        mOpenCvCameraView.setCameraPermissionGranted();
        // Codigo opcional para verificar Open CV
        if (!OpenCVLoader.initDebug()) {
            Log.e(this.getClass().getSimpleName(), "OpenCVLoader.initDebug(), not working.");
            Toast.makeText(this, "OpenCV not working.", Toast.LENGTH_LONG).show();
        } else {
            Log.d(this.getClass().getSimpleName(), "OpenCVLoader.initDebug(), working.");
            Toast.makeText(this, "OpenCV working.", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        // Llamamos a la inicializacion asincronica del objeto callback
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mLoaderCallback);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.i(TAG, "Creamos las opciones del menú");
        // Creamos las opciones del menu en ejecucion
        mItemPreviewRGBA = menu.add("Preview RGBA");
        mItemPreviewGray = menu.add("Preview GRAY");
        mItemPreviewCanny = menu.add("Canny");
        mItemPreviewEdges = menu.add("Edges");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item == mItemPreviewRGBA) {
            mViewMode = VIEW_MODE_RGBA;
        } else if (item == mItemPreviewGray) {
            mViewMode = VIEW_MODE_GRAY;
        } else if (item == mItemPreviewCanny) {
            mViewMode = VIEW_MODE_CANNY;
        } else if (item == mItemPreviewEdges){
            mViewMode = VIEW_MODE_EDGES;
        }
        return true;
    }

    // implementacion de CameraBridgeViewBase.CvCameraViewListener2
    @Override
    public void onCameraViewStarted(int width, int height) {
        // parametrizacion del analisis
        mRgba = new Mat(height, width, CvType.CV_8UC4);
        mIntermediateMat = new Mat(height, width, CvType.CV_8UC4);
        mGray = new Mat(height, width, CvType.CV_8UC1);
    }

    @Override
    public void onCameraViewStopped() {
        mRgba.release();
        mGray.release();
        mIntermediateMat.release();
    }

    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        // Retorna los frames dependiendo del modo seleccionado
        switch (mViewMode) {
            case VIEW_MODE_RGBA:
                mRgba = inputFrame.rgba();
                break;
            case VIEW_MODE_CANNY:
                Imgproc.Canny(inputFrame.gray(), mIntermediateMat, 80, 100);
                Imgproc.cvtColor(mIntermediateMat, mRgba, Imgproc.COLOR_GRAY2RGBA, 4);
                break;
            case VIEW_MODE_EDGES:
                /*Identifies and draws lines in the image using Canny edge filter.*/
                Mat lines = new Mat();
                int threshold = 50;
                Imgproc.Canny(inputFrame.gray(), mIntermediateMat, 80, 100);
                Imgproc.HoughLinesP(mIntermediateMat, lines, 1, Math.PI/180, threshold);
                Imgproc.cvtColor(mIntermediateMat, mIntermediateMat, Imgproc.COLOR_GRAY2RGB);
                for (int i = 0; i < lines.cols(); i++)
                {
                    double[] line = lines.get(0, i);
                    double xStart = line[0],
                            yStart = line[1],
                            xEnd = line[2],
                            yEnd = line[3];
                    org.opencv.core.Point lineStart = new org.opencv.core.Point(xStart, yStart);
                    org.opencv.core.Point lineEnd = new org.opencv.core.Point(xEnd, yEnd);
                    Imgproc.line(mIntermediateMat, lineStart, lineEnd, new Scalar(0, 0, 255), 3);
                    mRgba = mIntermediateMat;
                }
                break;
            case VIEW_MODE_GRAY:
            default:
                Imgproc.cvtColor(inputFrame.gray(), mRgba, Imgproc.COLOR_GRAY2RGBA, 4);
                break;
        }
        /*Rotates the image according to device orientation*/
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
            Mat mRgbaT = mRgba.t();
            Core.flip(mRgba.t(), mRgbaT, 1);
            Imgproc.resize(mRgbaT, mRgbaT, mRgba.size());
            return mRgbaT;
        }else{
            return mRgba;
        }
    }
}